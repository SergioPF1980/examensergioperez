page 80100 "TCN_EntradaDatos"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;


    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field("Texto1"; xArrayOfTexto[1])
                {

                    Caption = 'Mi Texto 1';
                    Visible = xVisText1;
                    CaptionClass = xArrayOfDatos[1];
                }
                field("Texto2"; xArrayOfTexto[2])
                {

                    Caption = 'Mi Text 2';
                    Visible = xVisText2;
                    CaptionClass = xArrayOfDatos[2];
                }

                field("Numero1"; xArrayOfNum[1])
                {

                    Caption = 'Mi Numero 1';
                    Visible = xVisNum1;
                    CaptionClass = xArrayOfDatos[3];
                }

                field("Numero2"; xArrayOfNum[2])
                {

                    Caption = 'Mi Numero 2';
                    Visible = xVisNum2;
                    CaptionClass = xArrayOfDatos[4];
                }

                field("Fecha1"; xArrayOfFecha[1])
                {

                    Caption = 'Mi Fecha 1';
                    Visible = xVisFecha1;
                    CaptionClass = xArrayOfDatos[5];
                }
                field("Fecha2"; xArrayOfFecha[2])
                {

                    Caption = 'Mi Fecha 2';
                    Visible = xVisFEcha2;
                    CaptionClass = xArrayOfDatos[6];
                }
            }
        }
    }

    trigger OnInit()
    var
        xInicioText: Decimal;
        xInicioNum: Decimal;
        xInicioFecha: Decimal;
    begin
        xInicioText := 2;
        xInicioNum := 2;
        xInicioNum := 2;
    end;

    var
        //  Variables Campos
        xText: Decimal;

        xNum: Decimal;

        xFecha: Decimal;

        //  Variables Visible
        xVisText1: Boolean;
        xVisText2: Boolean;
        xVisNum1: Boolean;
        xVisNum2: Boolean;
        xVisFecha1: Boolean;
        xVisFEcha2: Boolean;


        xArrayOfDatos: array[6] of Text;
        xArrayOfTexto: Array[2] of Text;
        xArrayOfNum: Array[2] of Decimal;
        xArrayOfFecha: Array[2] of Date;

        // xArrayOfContador: Array[6] of Integer; ERROR, Hay pasar 2 arrays segun SET y GET
        xArrayContSet: array[3] of Integer;
        xArrayContGet: array[3] of Integer;

    procedure PasarCaptionF(pCampo: Integer; pCaption: Text)
    var

    begin
        xArrayOfDatos[pCampo] := pCaption;
        case pCampo of
            1:
                begin
                    xVisText1 := true;
                end;
            2:
                begin
                    xVisText2 := true;
                end;
            3:
                begin
                    xVisNum1 := true;
                end;
            4:
                begin
                    xVisNum2 := true;
                end;
            5:
                begin
                    xVisFecha1 := true;
                end;
            6:
                begin
                    xVisFEcha2 := true;
                end;
        end;
    end;

    /*------------------------------------- SET -------------------------------------*/
    // procedure SetDatosF(pCaption: Text; pTipo: Text)
    // var
    //     xlCont: Integer;
    // begin
    //     xlCont := 0;
    //     if xlCont <= 2 then begin
    //         xArrayOfTexto[xlCont] := StrSubstNo('3,%1', pCaption);
    //         xArrayOfDatos[xlCont] := pTipo;
    //         case xlCont of
    //             1:
    //                 begin
    //                     xVisText1 := true;
    //                 end;
    //             2:
    //                 begin
    //                     xVisText2 := true;
    //                 end;
    //         end;
    //         xlCont += 1;
    //     end else begin
    //         xlCont := 1;
    //     end;

    // end;
    procedure SetDatosF(pCaption: Text; pTipo: Text)
    var

    begin
        if xArrayContSet[1] < xText then begin
            xArrayContSet[1] += 1;  //Sumamos 1 al contador
            PasarCaptionF(xArrayContSet[1], pCaption);
        end;

    end;

    procedure SetDatosF(pCaption: Text; pTipo: Decimal)
    var

    begin
        if xArrayContSet[2] < xNum then begin
            xArrayContSet[2] += 1;
            PasarCaptionF(xArrayContSet[2] + xText, pCaption);
        end;

    end;

    procedure SetDatosF(pCaption: Text; pTipo: Date)
    var

    begin
        if xArrayContSet[3] < xText then begin
            xArrayContSet[3] += 1;
            PasarCaptionF(xArrayContSet[3] + xText + xNum, pCaption);
        end;
    end;
    /*------------------------------------- GET -------------------------------------*/
    procedure GetDatosF(var ptipo: text) xSalida: Boolean
    var

    begin
        if xArrayContGet[1] < xText then begin
            xArrayContGet[1] += 1;
            ptipo := xArrayOfTexto[xArrayContGet[1]];
        end;
    end;

    procedure GetDatosF(var ptipo: Decimal) xSalida: Boolean
    var
    begin
        if xArrayContGet[2] < xNum then begin
            xArrayContGet[2] += 1;
            ptipo := xArrayOfNum[xArrayContGet[2]];
        end;
    end;

    procedure GetDatosF(var ptipo: Date) xSalida: Boolean
    var
    begin
        if xArrayContGet[3] < xFecha then begin
            xArrayContGet[3] += 1;
            ptipo := xArrayOfFecha[xArrayContGet[3]];
        end;
    end;
}