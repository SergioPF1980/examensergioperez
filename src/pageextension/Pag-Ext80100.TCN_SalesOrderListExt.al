pageextension 80100 "TCN_SalesOrderListExt" extends "Sales Order List"
{
    actions
    {
        addlast(Navigation)
        {


            action(Datos)
            {
                Caption = 'Ejercicio 2';

                trigger OnAction()

                var
                    culImportacionDAtos: Codeunit TCN_ImportacionDAtos;
                begin
                    culImportacionDAtos.Run();
                end;
            }

        }
    }
}