codeunit 80100 "TCN_ImportacionDAtos"
{
    Permissions = tabledata "Sales Header" = rmdi,
                tabledata "Sales Line" = rmdi;
    trigger OnRun()

    var
        rlTempBlobTMP: Record TempBlob temporary;
        rlTempCommentLine: Record "Comment Line" temporary; // tabla temporal para las incidencias

        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        rlCustomer: Record customer;

        plEntradaDatos: Page TCN_EntradaDatos;

        xlInStream: InStream;
        xlFichero: Text;
        xllinea: Text;
        xlCliente: Text;
        xlCodCliente: Code[10];
        xlSeparador: Text;
        xlCampo: Text;
        // xlNumCampo: Integer;
        xlCabecera: Boolean;

        xlContLinea: Integer;

    begin
        // xlCliente := '1005';
        //  rlTempBlobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);

        // if UploadIntoStream('Seleccione el fichero', '',
        //                      'Archivos de permitidos(*.txt,*.csv)|*.txt;*.csv| Todos los archivos (*.*)|*.*',
        //                      xlFichero, xlInStream) then begin

        //     while not xlInStream.EOS do begin
        //         xlInStream.ReadText(xlLinea);    //objeto de tipo lista
        //         xlNumCampo := 0; // el campo es cero, no ha leido ninguno
        //         rlSalesHeader.Init();
        //         foreach xlCampo in xlLinea.Split('·') do begin
        //             xlInStream.ReadText(xllinea);
        //             xlNumCampo += 1;

        //             case xlNumCampo of
        //                 1:
        //                     begin
        //                         rlSalesHeader.Validate("Order Date");
        //                     end;
        //             end;

        //         end;//Final del foreach
        //     end;//Final del While

        // end;    // Final del primer IF

        /*----------------------------------------------------------------------------------*/
        rlTempBlobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);
        plEntradaDatos.SetDatosF(xlCliente, 'Cod Cliente');
        plEntradaDatos.SetDatosF(xlSeparador, '·');

        if plEntradaDatos.RunModal() in [action::LookupOK, action::OK] then begin
            plEntradaDatos.GetDatosF(xlCliente);
            plEntradaDatos.GetDatosF(xlSeparador);
        end;    //Fin del primer IF

    end;
}